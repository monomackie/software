#include "StatusServer.h"
#include "SoundController.h"
#include "Log.h"
#include "mserialProtocol.h"
#include "SerialClient.h"
#include "Infotag.h"
#include "FrontInteraction.h"
#include "ConfigLoader.h"
#include "SoundSessionManager.h"

//char port[] = "COM ";
int baudrate = 115200;
int numberOfBits = 8;
int stopBits = 1;
SerialPortParity parity = SerialPortParity::SPP_NO_PARITY;
bool flowControl = false;
unsigned int timeout = 1000;
int selectedSession = -1;

void manageDeviceData(SerialData &serialData, SoundController &soundController, SoundSessionManager &ssm) {
    int device = serialData.device;
    if (serialData.type == HW_POTENTIOMETER_FUNCCODE) {
        float volume = ((float) (signed int) serialData.data) / 1023.f;
        ssm.changeSessionVolume(device, volume, soundController);
    }
    else if (serialData.type == HW_SWITCH_FOCUS_FUNCCODE) {
        bool switchForward = (serialData.data == HW_SWITCH_FOCUS_FORWARD);
        ssm.changeSessionFocus(device, switchForward, soundController);
    }
    else if (serialData.type == FUNCTYPE_RESET_CONNECTION) {
        ssm.resetSessions();
    }
}

int main() {
	Log::info("<%s> Monomackie driver has started", INFOTAG_DRIVER);

    // Init
	ConfigLoader configLoader("../config/config.json");
	SerialClient *serialClient = new SerialClient(configLoader.getCOMPort());
	SoundController soundController(SPEAKER_MODE);
    SoundSessionManager soundSessionManager(serialClient);
    FrontInteraction frontInteraction(48651);

    // ~Dependency Injection
    frontInteraction.shareDeviceState(serialClient->getStateRef(), soundSessionManager.getDeviceSession());

#pragma warning (disable : 4068 )
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
    while (true) {
        frontInteraction.pollSockets();
        frontInteraction.checkForEvents();

        SerialData serialData = serialClient->routine();
        if(serialData.type!=FUNCTYPE_NONE_FLAG){//there is something to process
            manageDeviceData(serialData, soundController, soundSessionManager);
        }
        soundSessionManager.sendSessionsName(soundController);
    }
#pragma clang diagnostic pop
    soundController.closeDevice();
    return 0;
}

