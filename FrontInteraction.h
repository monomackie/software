#pragma once

#include <vector>
#include <ws2tcpip.h>
#include <string>
#include "json.hpp"
#include "SerialClient.h"
#include "SoundSessionManager.h"
#include "StatusServer.h"

class FrontInteraction : public StatusServer {
private:
    serialClientState*          m_SharedSerialClientState;
    serialClientState           m_CopiedSerialClientState; //for "event detection"
    struct DeviceSessionInfo*   m_DeviceSessionInfo;
    int                         m_DeviceSessionInfoSize;

    static nlohmann::json getDriverVersionResponse();
    nlohmann::json getStateMMResponse();
    nlohmann::json getVolumesResponse();
    nlohmann::json getWinSessionsResponse();
public:
    explicit FrontInteraction(int port);
    void shareDeviceState(serialClientState *serialClientState, struct DeviceSessionInfo *deviceSessionInfo);

    void processClientMessage(nlohmann::basic_json<> request) override;

    void checkForEvents();
};