# Software

This repository contains the code source of the main program of Monomackie.

Monomackie is a system that composed of both a physical device and a suite of integration on computer. The purpose of Monomackie is to make the management of the sound on a computer easy and fluent.

This program is the driver that make the bridge via USB (serial) for the device and via the WinApi in order to control the sound of the OS, from the several softwares.

## User

### Running

The executable is produced regularly and you can download it directly on Gitlab [here](https://gitlab.com/monomackie/software/-/jobs/artifacts/master/download?job=build-code-job).

Run it for the time you want to use Monomackie.

### Infos on device connection

It may be necessary to update the drivers of the computer ; windows update may do the job just fine ( check for "optional updates")

At the time the port in which the device have to be connected is hard-coded (defined @compilation), will be update soon !

### Config

The driver comes with a config file within the config folder.
The config file follows JSON specification. Here is an explanation of the config parameters :
```json
{
  "version": "1.0.0",       //placeholder for when a version management will be established
  "useConfigFile": true,    //true if you want to set config the driver using this file, false otherwise
  "comPort": "\\\\.\\COM4", //"auto" if you want to use autoconnect, device COM port name otherwise
  "forcedCursorSettings": [ //sets which cursor controls which session
    {
		"device":	0,        //id of the device
		"session": 	"firefox" //name of the session to associate with the device
	},
	{
		"device":	1,
		"session": 	"discord"
	},
	{
		"device":	2,
		"session": 	"vlc"
	}
  ]
}
```

## Dev

### Compiling

In order to retrieve the different libs required to compile this project, you need to run these commands in the root folder of the project :

```shell
git submodule init
git submodule update --remote
```

You will need to run with an environment properly set up, you should run the vcvarsall script file, that is located in somewhere like : 
```shell
C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Tools\MSVC\14.29.30133\bin\Hostx86\x86\
```

Then you can produce the makefiles with Cmake for the proper project solution (nmake) :

```shell
cmake.exe" -G "NMake Makefiles"
```

Finally compile it with MSVC :

```shell
nmake
```

### Misc : adding a submodule

To add a submodule, consider using: 
```shell
git submodule add -b branch -f --name name_of_the_lib git_url path_within_project
```

Finally, add the following lines to your CMakeLists.txt :
```shell
add_subdirectory(path_within_project)
target_include_directories(monomackie_software PUBLIC path_within_project)
target_link_libraries(monomackie_software name_of_the_lib)
```

