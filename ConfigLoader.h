#pragma once
#include "json.hpp"
#include "SerialClient.h"

class ConfigLoader {
private:
    nlohmann::json  m_Config;
    bool            m_ConfigEnabled;
	static nlohmann::json readJSONFile(const std::string& path);
public:
    explicit ConfigLoader(const std::string &pathToConfig);
    std::string getCOMPort();
};



