#include "FrontInteraction.h"
#include "Log.h"

FrontInteraction::FrontInteraction(int port) : StatusServer(port), m_SharedSerialClientState(nullptr),
                                               m_DeviceSessionInfo(nullptr), m_DeviceSessionInfoSize(0) {}

nlohmann::json FrontInteraction::getDriverVersionResponse() {
    nlohmann::json response;
    response["driverVersion"] = "0.0"; //TODO not implemented yet
    return response;
}

nlohmann::json FrontInteraction::getStateMMResponse() {
    nlohmann::json response;
    switch (*m_SharedSerialClientState) {
        case POLLING_PORTS:
            response["isMMConnected"] = "Recherche d'un périphérique";
            break;
        case CONNECTING:
            response["isMMConnected"] = "Établissement de la communication";
            break;
        case WORKING:
            response["isMMConnected"] = "Fonctionnement normal";
            break;
    }
    return response;
}

nlohmann::json FrontInteraction::getVolumesResponse() {
    nlohmann::json response;
    std::vector<float> sessionsVolumeVector;
    //sessionsVolumeVector.push_back(*singleVolumeRef);//TODO not really implemented, only one session level, waiting for modularity
    //sessionsVolumeVector.push_back(anotherVolumeRef);//TODO not implemented yet

    response["sessionVolumes"]= sessionsVolumeVector;
    return response;
}

nlohmann::json FrontInteraction::getWinSessionsResponse() {
    nlohmann::json response;
    std::vector<nlohmann::json> winSessions;

    //TODO not implemented yet
    winSessions.push_back(nlohmann::json{
            {"id", "idOfTheFirstSession"},
            {"name", "nameOfTheFirstSession"},
            {"icoPath", "path/to/ico"}
    });
    winSessions.push_back(nlohmann::json{
            {"id", "idOfTheSecondSession"},
            {"name", "nameOfTheSecondSession"},
            {"icoPath", "path/to/2ndico"}
    });

    response["winSessions"]= winSessions;

    return response;
}

void FrontInteraction::processClientMessage(nlohmann::basic_json<> request) {
    if ((request.contains("ask"))) {
        if(request["ask"] == "DriverVersion"){
            sendToClient(getDriverVersionResponse());
            return;
        }
        if(request["ask"] == "StateMM"){
            sendToClient(getStateMMResponse());
            return;
        }
        if(request["ask"] == "Volumes") {
            sendToClient(getVolumesResponse());
            return;
        }
        if(request["ask"] == "WinSessions") {
            sendToClient(getWinSessionsResponse());
            return;
        }

        Log::error("StatusServer received a request from client with an unknown value");
        return;
    }
    Log::error("StatusServer received a message from client without a request");
}

void FrontInteraction::shareDeviceState(serialClientState *serialClientState, struct DeviceSessionInfo *deviceSessionInfo) { //TODO const ref is better
    m_SharedSerialClientState = serialClientState;
    m_DeviceSessionInfo = deviceSessionInfo;
}

void FrontInteraction::checkForEvents() {
    if(*m_SharedSerialClientState != m_CopiedSerialClientState){
        m_CopiedSerialClientState = *m_SharedSerialClientState;
        sendToClient(getStateMMResponse());
    }
}
