#include "StatusServer.h"
#include "Log.h"

#pragma comment (lib, "Ws2_32.lib")

#define INPUT_BUFFER_MAX_SIZE 1024

sockaddr_in addr = {0};

StatusServer::StatusServer(int port) {
    bindSocket(port);
}

SOCKET StatusServer::initWinSocket() {
    WSAData wsaData{};
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
        Log::error("Error initializing WinSock");
    }

    SOCKET server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (server == INVALID_SOCKET) {
        Log::error("Error initializing");
    }
    return server;
}

void StatusServer::bindSocket(unsigned short port) {
    SOCKET server = initWinSocket();
    inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr.s_addr);
    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;
    int res = bind(server, (sockaddr *) &addr, sizeof(addr));
    if (res != 0) {
        Log::error("Error while binding");
    }
    res = listen(server, SOMAXCONN);
    if (res != 0) {
        Log::error("Error while listening");
    }
    listeningSocketFD[0].fd = server;
    listeningSocketFD[0].events = POLLIN;
    listeningSocketFD[0].revents = 0;
}

/* //TODO use it as a clean close
void SocketServer::closeServer(SOCKET server) {
    closesocket(server);
    WSACleanup();
}
*/

void StatusServer::handleNewConnectionRequest() {
    if(pollLength < 2) {
        Log::info("New client connection");
        SOCKET newClient = accept(listeningSocketFD[0].fd, nullptr, nullptr);
        listeningSocketFD[1].fd = newClient;
        listeningSocketFD[1].events = POLLIN;
        listeningSocketFD[1].revents = 0;
        pollLength = 2;
    }
    else{
        Log::info("Client discarded because of duplicate");
        SOCKET newClient = accept(listeningSocketFD[0].fd, nullptr, nullptr);
        //LATER send "close because already connected"
        closesocket(newClient);
    }
}

void StatusServer::handleClientRequest() {
    char inputBuffer[INPUT_BUFFER_MAX_SIZE];
    int bytesReceived = recv(listeningSocketFD[1].fd, inputBuffer, INPUT_BUFFER_MAX_SIZE, 0);
    if(bytesReceived>INPUT_BUFFER_MAX_SIZE-2){
        Log::error("Status request does not comply with the size of the buffer");
        /*
         * We cannot "flush" the win-socket, so let the json parser fail with the next part(s) of the request
         * https://stackoverflow.com/questions/12814835/flush-a-socket-in-c
         * Another way would be to identify the begin of a request with a special character, too complicated for now
         */
        return;
    }
    inputBuffer[bytesReceived] = '\0';
    try
    {
        nlohmann::json receivedJson = nlohmann::json::parse(inputBuffer);
        processClientMessage(receivedJson);
    }
    catch (nlohmann::json::parse_error& e) {
        Log::error("Could not parse json from request front client");
        Log::printStackTrace(e);
    }
}

void StatusServer::sendToClient(const nlohmann::json& message){
    if (pollLength < 2) {
        return;
    }

    std::string responseString = message.dump();
    send(listeningSocketFD[1].fd, responseString.c_str(), (int) responseString.length(), 0);
}

void StatusServer::pollSockets() {
    if (WSAPoll(listeningSocketFD, pollLength, 0) > 0) {
        if ((listeningSocketFD[0].revents & POLLIN) != 0) {
            handleNewConnectionRequest();
        }
        if (pollLength > 1) {
            if ((listeningSocketFD[1].revents & POLLHUP) != 0) {
                //Client socket is lost : deconnection
                Log::info("Client disconnection");
                pollLength = 1;
                closesocket(listeningSocketFD[1].fd);
            }
            else if ((listeningSocketFD[1].revents & POLLIN) != 0) {
                //Client socket : read
                handleClientRequest();
            }
        }
    }
}