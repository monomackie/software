#include "ConfigLoader.h"
#include <fstream>
#include <Log.h>
#include <Exception.h>
#include "Infotag.h"

ConfigLoader::ConfigLoader(const std::string& path) : m_Config(nlohmann::json::value_t::object), m_ConfigEnabled(false) {
    try {
        m_Config = readJSONFile(path);

        if (m_Config.contains("useConfigFile") && m_Config["useConfigFile"]) {
            m_ConfigEnabled = true;
            Log::info("<%s> Loading config from file: %s", INFOTAG_CONFIG_LOADER, path.c_str());
        }
    } catch (const std::exception& e) {
        Log::printStackTrace(e);
    }
}

nlohmann::json ConfigLoader::readJSONFile(const std::string& path) {
	std::ifstream inputFile(path);
	if (!inputFile.good()) {
		Log::warning("Config file not found at: %s", path.c_str());
	}
	else {
		try {
			nlohmann::json j;
			inputFile >> j;
			return j;
		} catch (const std::exception& e) {
			THROW_EXCEPTION("Error while parsing JSON file: " + path);
		}
	}

	return nlohmann::json::value_t::object;
}

std::string ConfigLoader::getCOMPort() {
    if (m_ConfigEnabled) {
        if (m_Config.contains("comPort") && ((std::string) m_Config["comPort"]) != "auto") {
            std::string comPort = m_Config["comPort"];
            return comPort;
        }
        else {
            return "auto";
        }
    }
    else {
        return "auto";
    }
}
