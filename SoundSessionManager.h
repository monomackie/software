#pragma once

#include <SoundController.h>
#include "SerialClient.h"

#define MAX_SESSIONS 256
#define MAX_DEVICES  50
#define SSM_SESSION_NOT_INSTANCIATED -3
#define SSM_SESSION_NOT_ASSIGNED     -2
#define SSM_SESSION_MASTER           -1

struct DeviceSessionInfo {
    /**
     * selectedSessions = SSM_SESSION_NOT_INSTANCIATED => not instanciated,
     * selectedSessions = SSM_SESSION_NOT_ASSIGNED => assigned to no windows session,
     * selectedSessions = SSM_SESSION_MASTER => master session
     * selectedSessions = [0 -> MAX_SESSIONS-1] => application session
     */
    int selectedSession;
    float sessionVolume;
    bool sessionHasChanged;
};

class SoundSessionManager {
private:
    /**
     * m_DeviceSessionInfo[device] = info about audio session linked to device
     */
    struct DeviceSessionInfo    m_DeviceSessionInfo[MAX_DEVICES];
    SerialClient*               m_SerialClient;
    void getOccupiedSessions(SoundController &soundController, bool **occupiedSessions);
    int findFirstAvailableSession(SoundController &soundController);
    int findNextAvailableSession(bool forward, int device, SoundController &soundController);
public:
    explicit SoundSessionManager(SerialClient* serialClient);
    struct DeviceSessionInfo* getDeviceSession();
    void sendSessionsName(SoundController &soundController);
    void changeSessionVolume(int device, float volume, SoundController &soundController);
    void changeSessionFocus(int device, bool forward, SoundController &soundController);

    void resetSessions();

    std::string getSessionNameFromID(int id, SoundController &soundController);
};
