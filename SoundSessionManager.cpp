#include "SoundSessionManager.h"
#include "SerialClient.h"
#include "mserialProtocol.h"
#include "Log.h"
#include "Infotag.h"

SoundSessionManager::SoundSessionManager(SerialClient* serialClient) : m_DeviceSessionInfo {}, m_SerialClient(serialClient) {
    resetSessions();
}

/**
 * Fills occupiedSessions. Example: occupiedSessions[windowsSession+1] = false
 * @param soundController
 * @param occupiedSessions pointer to the bool array we want to fill
 */
void SoundSessionManager::getOccupiedSessions(SoundController &soundController, bool** occupiedSessions) {
    soundController.updateSessionEnumerator();
    int windowsSessionsCount = soundController.getSessionCount();
    *occupiedSessions = (bool*) malloc((windowsSessionsCount+1) * sizeof(bool));
    for (int i = 0 ; i < windowsSessionsCount+1 ; i ++) {
        (*occupiedSessions)[i] = false;
    }
    for (int i = 0 ; i < MAX_DEVICES ; i++) {
        if (m_DeviceSessionInfo[i].selectedSession >= SSM_SESSION_MASTER && m_DeviceSessionInfo[i].selectedSession < windowsSessionsCount) {
            (*occupiedSessions)[m_DeviceSessionInfo[i].selectedSession + 1] = true;
        }
    }
}

/**
 * @return first windows session which is not currently controlled by a device
 * or SSM_SESSION_NOT_ASSIGNED if no session is available
 */
int SoundSessionManager::findFirstAvailableSession(SoundController &soundController) {
    int windowsSessionsCount = soundController.getSessionCount();
    bool *occupiedSessions;
    getOccupiedSessions(soundController, &occupiedSessions);

    int availableSession = SSM_SESSION_NOT_ASSIGNED;
    for (int i = 0 ; i < windowsSessionsCount+1 ; i++) {
        if (!occupiedSessions[i]) {
            availableSession = i-1; //shift -1 because first session is -1 (master volume)
            break;
        }
    }
    free(occupiedSessions);
    return availableSession;
}

/**
 * Finds an available session (i.e not currently controlled by a device)
 * starting from the specified index and either looking forward or backward
 * in the list of sessions
 * @param forward controls whether we look forward in the list or backward
 * @param device index from which we start to search for an available session
 * @param soundController
 * @return the available windows session or -2 if no session was found
 */
int SoundSessionManager::findNextAvailableSession(bool forward, int device, SoundController &soundController) {
    int windowsSessionsCount = soundController.getSessionCount();
    bool *occupiedSessions;
    getOccupiedSessions(soundController, &occupiedSessions);

    int availableSession = SSM_SESSION_NOT_ASSIGNED;
    for (int i = 0 ; i < windowsSessionsCount+1 ; i++) {
        int j = 0;
        if (forward)
            j = (m_DeviceSessionInfo[device].selectedSession + i);
        else
            j = (m_DeviceSessionInfo[device].selectedSession - i);
        while (j > windowsSessionsCount) //probably should be if, but I'm scared to try
            j-= windowsSessionsCount+1;
        while (j < 0)
            j+= windowsSessionsCount+1;
        if (!occupiedSessions[j]) {
            availableSession = j-1;
            break;
        }
    }
    free(occupiedSessions);
    return availableSession;
}

void SoundSessionManager::changeSessionVolume(int device, float volume, SoundController &soundController) {
    if (m_DeviceSessionInfo[device].selectedSession == SSM_SESSION_NOT_INSTANCIATED) { //give a session to a new device
        m_DeviceSessionInfo[device].selectedSession = findFirstAvailableSession(soundController);
        m_DeviceSessionInfo[device].sessionHasChanged = true;
    }
    m_DeviceSessionInfo[device].sessionVolume = volume;
    if (m_DeviceSessionInfo[device].selectedSession == SSM_SESSION_NOT_ASSIGNED) //no session available for this device
        return;
    else if (m_DeviceSessionInfo[device].selectedSession == SSM_SESSION_MASTER)
        soundController.setMasterVolume(volume);
    else {
        soundController.setSessionVolume(m_DeviceSessionInfo[device].selectedSession, volume);
    }
}

void SoundSessionManager::changeSessionFocus(int device, bool forward, SoundController &soundController) {
    m_DeviceSessionInfo[device].sessionHasChanged = true;
    if (m_DeviceSessionInfo[device].selectedSession == SSM_SESSION_NOT_INSTANCIATED) { //give a session to a new device
        m_DeviceSessionInfo[device].selectedSession = findFirstAvailableSession(soundController);
    }
    m_DeviceSessionInfo[device].selectedSession = findNextAvailableSession(forward, device, soundController);
    std::vector<std::string> names = soundController.getSessionsName();

    Log::info("<%s> Selected session '%s' by device %d", INFOTAG_SELECTED_SESSION, getSessionNameFromID(m_DeviceSessionInfo[device].selectedSession, soundController).c_str(), device);
}

std::string SoundSessionManager::getSessionNameFromID(int id, SoundController &soundController) {
    switch (id) {
        case -3:
            return "uninstanciated";
        case -2:
            return "Unassigned";
        case -1:
            return "Master volume";
        default: {
            std::vector<std::string> names = soundController.getSessionsName();
            return names[id];
        }
    }
}

void SoundSessionManager::sendSessionsName(SoundController &soundController) {
    for (int i = 0 ; i < MAX_DEVICES ; i++) {
        if (m_DeviceSessionInfo[i].sessionHasChanged) {
            m_DeviceSessionInfo[i].sessionHasChanged = false;
            std::vector<std::string> names = soundController.getSessionsName();

            if (m_DeviceSessionInfo[i].selectedSession != SSM_SESSION_NOT_INSTANCIATED) { //we only care about instanciated device
                m_SerialClient->sendSessionNames(getSessionNameFromID(m_DeviceSessionInfo[i].selectedSession, soundController), i);
            }
        }
    }
}

void SoundSessionManager::resetSessions() {
    for (int i = 0 ; i < MAX_DEVICES ; i++) {
        m_DeviceSessionInfo[i].sessionHasChanged = true;
        m_DeviceSessionInfo[i].selectedSession = SSM_SESSION_NOT_INSTANCIATED;
        m_DeviceSessionInfo[i].sessionVolume = 0.5f;
    }
}

struct DeviceSessionInfo* SoundSessionManager::getDeviceSession() {
    return m_DeviceSessionInfo;
}

