#include "SerialClient.h"
#include "mserialProtocol.h"
#include "Log.h"
#include "Infotag.h"
#include <ctime>

SerialClient::SerialClient(const std::string& port, int baudRate, int numberOfBits, int stopBits, SerialPortParity parity, bool flowControl, unsigned int timeout) :
        m_Baudrate(baudRate),
        m_NumberOfBits(numberOfBits),
        m_StopBits(stopBits),
        m_Parity(parity),
        m_FlowControl(flowControl),
        m_Timeout(timeout),
        m_State(POLLING_PORTS),
        m_COMPortIsImposed(true),
        m_ImposedPort(port),
        m_HeartBeatEmissionDate(0),
        m_HeartBeatReceived(false),
        m_CurrentCOMPortSearch(0) {

    if (m_ImposedPort == "auto") {
        m_COMPortIsImposed = false;
        Log::info("<%s> Using autoconnect to find available COM port", INFOTAG_DEVICE_CONNECTING);
    }
    else {
        Log::info("<%s> Connecting on COM port: %s", INFOTAG_DEVICE_CONNECTING, port.c_str());
    }
} //init in connecting mode if a COM port is specified

bool SerialClient::isCOMPortValid(const std::string &portName, unsigned int timeout_ms, int baudRate, int bits, int stopBits, SerialPortParity parity, bool flowControl, SerialPort** result) {
    bool portIsValid = true;
    SerialPort* serialPort;

    try {
        serialPort = new SerialPort(portName, timeout_ms, baudRate, bits, stopBits, parity, flowControl);
    }
    catch (...) {
        portIsValid = false;
    }

    if (portIsValid) {
        (*result) = serialPort;
    }
    return portIsValid;
}

SerialPort *SerialClient::findAvailableCOMPort(int maxPortNumber) {
    for (m_CurrentCOMPortSearch ; m_CurrentCOMPortSearch < maxPortNumber ; m_CurrentCOMPortSearch++) {
        std::string port = R"(\\.\COM)" + std::to_string(m_CurrentCOMPortSearch);
        SerialPort* serialPort;
        if (isCOMPortValid(port, m_Timeout, m_Baudrate, m_NumberOfBits, m_StopBits, m_Parity, m_FlowControl, &serialPort)) {
            m_CurrentCOMPortSearch++;
            return serialPort;
        }
    }
    m_CurrentCOMPortSearch = 0;
    return nullptr;
}

bool SerialClient::isDevicePinging(SerialPort* port) {
    unsigned char input;
    for (int i = 0 ; i < 2 ; i++) {
        input = 0;
        if (port->readByte(&input) != 1 || input != HW_PING_TO_HOST_FUNCCODE) {
            return false;
        }
    }
    return true;
}

SerialData SerialClient::listenForData() {
    unsigned char input;
    SerialData sd = {FUNCTYPE_NONE_FLAG, 0};
    if (m_SerialPort->readByte(&input) > 0) {
        sd.type = input;

        switch (input) {
            case HW_POTENTIOMETER_FUNCCODE: {
                unsigned char potentiometre[HW_POTENTIOMETER_SIZEVAL];
                m_SerialPort->readBytes(potentiometre, HW_POTENTIOMETER_SIZEVAL);
                sd.device = potentiometre[0];
                sd.data = (potentiometre[1] << 8) + potentiometre[2];
                break;
            }
            case HW_SWITCH_FOCUS_FUNCCODE: {
                unsigned char buffer[HW_SWITCH_FOCUS_SIZEVAL];
                m_SerialPort->readBytes(buffer, HW_SWITCH_FOCUS_SIZEVAL);
                sd.device = buffer[0];
                sd.data = buffer[1];
                break;
            }
			case HW_PONG_TO_HOST_FUNCCODE: {
                m_HeartBeatReceived = true;
				break;
			}
			case HW_PING_TO_HOST_FUNCCODE: {
				unsigned char output[] = {SW_PONG_TO_DEVICE_FUNCCODE};
				m_SerialPort->writeByte(output);
				break;
			}
            default:
                //Log::debug("Unknown serial data: %d", input);
                break;
		}
    }
    return sd;
}

/**
 * sends heartbeat every second and check if response has been received
 * @return true when device is responding, false otherwise
 */
bool SerialClient::isDeviceStillAlive() {
	if (time(nullptr) >= m_HeartBeatEmissionDate + HEARTBEAT_PERIOD) {
		if (m_HeartBeatReceived) {
			unsigned char output[] = {SW_PING_TO_DEVICE_FUNCCODE};
            m_HeartBeatEmissionDate = time(nullptr);
            m_SerialPort->writeByte(output);
            m_HeartBeatReceived = false;
			return true;
		}
		else {
			return false;
		}
	}
	return true;
}

SerialData SerialClient::routine() {
    switch(m_State) {
        case POLLING_PORTS: {
            if (m_COMPortIsImposed) {
                SerialPort* imposedPort;
                if (isCOMPortValid(m_ImposedPort, m_Timeout, m_Baudrate, m_NumberOfBits, m_StopBits, m_Parity, m_FlowControl, &imposedPort)) {
                    m_State = CONNECTING;
                    m_SerialPort = imposedPort;
                }
            }
            else {
                SerialPort* availablePort = findAvailableCOMPort(15);
                if(availablePort != nullptr) {
                    m_SerialPort = availablePort;
                    m_State = CONNECTING;
                }
            }
            return {FUNCTYPE_NONE_FLAG, 0, 0};
        }

        case CONNECTING: {
            if(isDevicePinging(m_SerialPort)) {
                unsigned char output[] = {SW_PONG_TO_DEVICE_FUNCCODE};
                m_SerialPort->writeByte(output);
                m_State = WORKING;
                m_HeartBeatReceived = true;
                Log::info("<%s> Connection established with device on port %s", INFOTAG_DEVICE_CONNECTED, m_SerialPort->getPortName().c_str());
            }
            else if (!m_COMPortIsImposed) {
                m_State = POLLING_PORTS;
				delete(m_SerialPort);
            }
            return {FUNCTYPE_NONE_FLAG, 0, 0};
        }

        case WORKING: {
			if (isDeviceStillAlive()) {
				return listenForData();
			}
			else {
                Log::info("<%s> Connection lost with device on port %s", INFOTAG_DEVICE_DISCONNECTED, m_SerialPort->getPortName().c_str());
				delete(m_SerialPort);
				m_State = POLLING_PORTS;
				return {FUNCTYPE_RESET_CONNECTION, 0, 0};
			}
        }
    }
    return {FUNCTYPE_NONE_FLAG, 0, 0};
}

serialClientState *SerialClient::getStateRef() {
    return &m_State;
}

void SerialClient::sendSessionNames(std::string sessionName, unsigned int deviceID) {
    unsigned char sessionNameChar[SW_SESSION_NAME_SIZEVAL+1];
    if(sessionName.length() > SW_SESSION_NAME_SIZEVAL-1) {
        sessionName=sessionName.substr(0,SW_SESSION_NAME_SIZEVAL-1);
    }
    for (int i = 0 ; i < SW_SESSION_NAME_SIZEVAL+1 ; i++) {
        sessionNameChar[i] = 0;
    }
    sessionNameChar[0] = SW_SESSION_NAME_FUNCODE ;
    sessionNameChar[1] = (unsigned char) deviceID;
    for (unsigned int i = 0 ; i < sessionName.length() ; i++) {
        sessionNameChar[i+2] = (unsigned char) sessionName[i];
    }

    m_SerialPort->writeBytes(sessionNameChar, SW_SESSION_NAME_SIZEVAL+1);
}
