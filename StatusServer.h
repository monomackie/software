#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include "json.hpp"

class StatusServer {
private:
    WSAPOLLFD listeningSocketFD[2] = {};
    int pollLength = 1;

    static SOCKET initWinSocket();
    void bindSocket(unsigned short port);

    void handleNewConnectionRequest();
    void handleClientRequest();

public:
    explicit StatusServer(int port);

    void pollSockets();

    void sendToClient(const nlohmann::json &message);

    virtual void processClientMessage(nlohmann::basic_json<> request) = 0;
};