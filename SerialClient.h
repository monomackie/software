#pragma once

#include "SerialPort.h"

#define FUNCTYPE_NONE_FLAG          0 //because mserial does not use functype = 0
#define FUNCTYPE_RESET_CONNECTION	256 //outside mserial range

#define HEARTBEAT_PERIOD 2

enum serialClientState {POLLING_PORTS, CONNECTING, WORKING};

struct SerialData {
    int type;
    int device;
    unsigned int data;
};


class SerialClient {
private:
	int 				m_Baudrate;
	int 				m_NumberOfBits;
	int 				m_StopBits;
	SerialPortParity 	m_Parity;
	bool 				m_FlowControl;
	unsigned int 		m_Timeout;

	bool				m_COMPortIsImposed;
	std::string	 		m_ImposedPort;
	serialClientState 	m_State;
	SerialPort			*m_SerialPort = nullptr;
	long long int		m_HeartBeatEmissionDate;
	bool 				m_HeartBeatReceived;
    int                 m_CurrentCOMPortSearch;

	static bool isDevicePinging(SerialPort* port);
    SerialData listenForData();

	bool isDeviceStillAlive();
	SerialPort *findAvailableCOMPort(int maxPortNumber);
	static bool isCOMPortValid(const std::string &portName, unsigned int timeout_ms, int baudRate,
				   int bits,
				   int stopBits, SerialPortParity parity, bool flowControl, SerialPort **result);
public:
	explicit SerialClient(const std::string& port, int baudrate=115200, int numberOfBits=8, int stopBits=1, SerialPortParity parity=SerialPortParity::SPP_NO_PARITY, bool flowControl=false, unsigned int timeout=1000);
	SerialData routine();
    serialClientState* getStateRef();
    void sendSessionNames(std::string sessionName, unsigned int deviceID);
};
